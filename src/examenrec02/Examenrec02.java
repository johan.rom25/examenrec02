/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrec02;

/**
 *
 * @author ionix
 */
public class Examenrec02 {

    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int diasTrabajados;
    
    public Examenrec02(){
        
    }

    public Examenrec02(int numRecibo, String nombre, int puesto, int nivel, int diasTrabajados) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.diasTrabajados = diasTrabajados;
    }
    
    public Examenrec02(Examenrec02 otro) {
      this.puesto = otro.puesto;
        this.nivel = otro.nivel;
        this.nombre = otro.nombre;
        this.numRecibo = otro.numRecibo;
        this.diasTrabajados = otro.diasTrabajados;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    public float calcularPago(){
        float pago = 0.0f;
        if (this.puesto == 1){
           pago= this.diasTrabajados * 100;
       }
       if (this.puesto == 2){
           pago= this.diasTrabajados * 200;
       }
       if (this.puesto == 3){
           pago= this.diasTrabajados * 300;
       }
        return pago;
    }
    
    public float calcularImpuesto(){
       float impuesto = 0.0f;
       if (this.nivel == 1){
           impuesto = this.calcularPago() * 0.05f;
       }
       if (this.nivel == 2){
           impuesto = this.calcularPago() * 0.03f;
       }
       return impuesto;
    }
    
    public float calcularPagoTotal(){
       float total = 0.0f;
       total = this.calcularPago() - this.calcularImpuesto();
       return total;
    }
}
